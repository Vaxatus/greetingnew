import helper

if __name__ == "__main__":
    helper.greet("Piotrek", 0)
    helper.greet("Kasia", 2) # usmiech :)
    helper.greet("Jacek", 1) # ladna pogoda

    helper.greet("Piotrek", 0)
    helper.greet("Kasia", 1) # ladna pogoda
    helper.greet("Jacek", 2) # usmiech :)